package woah.woah;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.beans.Transient;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */


    @Test
    public void willEchoEcho(){
        assertEquals("To see if echo echoes, or if it is lying to us.", 5, App.echo(5), 0);
    }

    @Test
    public void oneMoreTest(){
        assertEquals("Tests oneMore, to see if it realy does add one more.", 6, App.oneMore(5), 0);
    }
}
